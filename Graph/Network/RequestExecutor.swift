import Alamofire
import RxSwift
import RxCocoa

struct RawRequest {
    let path: URLConvertible
    let method: Alamofire.HTTPMethod
    let parameters: [String: Any]?
    let headers: [String: String]?
}


class RequestExecutor {

    private var sessionManager: Alamofire.SessionManager

    private let scheduler: ConcurrentDispatchQueueScheduler

    init(sessionManager: Alamofire.SessionManager) {
        self.sessionManager = sessionManager
        self.scheduler = ConcurrentDispatchQueueScheduler(qos: .userInitiated)
    }

    func perform<T: Decodable>(request: RawRequest) -> Observable<ResponseResult<T>>  {
        return executeRequest(configureRequest(by: request))
    }

}

private extension RequestExecutor {
    func configureRequest(by rawRequest: RawRequest) -> DataRequest {
        let request = sessionManager.request(rawRequest.path,
                                             method: rawRequest.method,
                                             parameters: rawRequest.parameters,
                                             encoding: URLEncoding.default,
                                             headers: rawRequest.headers)
        return request
    }

    func executeRequest<T: Decodable>(_ request: DataRequest) -> Observable<ResponseResult<T>> {
        print(request.debugDescription)
        return Observable
            .create({ observer -> Disposable in
                request
                    .validate(statusCode: 200..<300)
                    .responseData(completionHandler: { response in
                        do {
                            let result: ResponseResult<T> = try RequestExecutor.map(response.result)
                            observer.onNext(result)
                            observer.onCompleted()
                        } catch let error {
                            observer.onError(error)
                        }
                    })

                return Disposables.create {
                    request.cancel()
                }
            })
            .subscribeOn(scheduler)
    }

    static func map<T: Decodable>(_ result: Alamofire.Result<Data>) throws -> ResponseResult<T> {
        switch result {
        case let .success(value):
            do {
                let decoded = try JSONDecoder().decode(T.self, from: value)
                return .success(decoded)
            } catch {
                guard let decoded = try? JSONDecoder().decode(ErrorResponse.self, from: value) else {
                    throw PointsServiceError.unknown
                }

                return .serverError(decoded)
            }
        case let .failure(error):
            throw error
        }
    }
}
