import Foundation

private let errorsInBase64 = [-1]

struct ServerErrorTransformer {

    static func transform(error: ErrorResponse) -> ErrorResponse {
        switch error.result {
        case let result where errorsInBase64.contains(result):
            let message = error.message
            guard let data = Data(base64Encoded: message) else {
                return error
            }
            return ErrorResponse(result: result, message: String(data: data, encoding: .utf8) ?? "")
        default:
            return error
        }
    }
}
