import Alamofire


struct PolicyManagerProvider {

    static func makeServerTrustPolicyManager() -> ServerTrustPolicyManager {
        let url = Bundle.main.url(forResource: "bankplus", withExtension: "cer")!
        let certData = try! Data(contentsOf: url)

        let serverTrustPolicy = ServerTrustPolicy.pinCertificates(
            certificates: [SecCertificateCreateWithData(nil, certData as CFData)!],
            validateCertificateChain: true,
            validateHost: true
        )
        
        let serverTrustPolicies = [
            "demo.bankplus.ru": serverTrustPolicy
        ]

        let policyManager = ServerTrustPolicyManager(policies: serverTrustPolicies)
        return policyManager
    }
}
