import Foundation
import Alamofire
import RxSwift


protocol PointsServiceType {
    func getPoints(count: Int) -> Observable<ResponseResult<SuccessResponse>>
}

enum PointsServiceError: Error {
    case unknown
}

final class PointsService: NSObject {

    private let baseURL: URL

    private let requestExecutor: RequestExecutor

    init(with URL: URL, configuration: URLSessionConfiguration = URLSessionConfiguration.default) {
        self.baseURL = URL

        let sessionManager = Alamofire.SessionManager(
            configuration: configuration,
            serverTrustPolicyManager: PolicyManagerProvider.makeServerTrustPolicyManager())

        requestExecutor = RequestExecutor(sessionManager: sessionManager)
    }
}

extension PointsService: PointsServiceType {

    func getPoints(count: Int) -> Observable<ResponseResult<SuccessResponse>> {
        let path = baseURL.appendingPathComponent("json/pointsList")
        let parameters: [String: Any] = [
            "count": "\(count)",
            "version": "1.1"
        ]

        let request = RawRequest(path: path, method: .post, parameters: parameters, headers: nil)
        let result: Observable<ResponseResult<SuccessResponse>> = requestExecutor.perform(request: request)
        return result
            .map({ result -> ResponseResult<SuccessResponse> in
                switch result {
                case .success:
                    return result
                case let .serverError(error):
                    return ResponseResult.serverError(ServerErrorTransformer.transform(error: error))
                }
            })
    }

}
