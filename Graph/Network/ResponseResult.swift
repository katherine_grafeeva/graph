enum ResponseResult<T> {
    case success(T)
    case serverError(ErrorResponse)
}
