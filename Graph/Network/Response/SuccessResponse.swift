import Foundation


struct SuccessResponse: Decodable {
    let result: Int
    let response: PointsListResponse
}
