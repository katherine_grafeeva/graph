import Foundation


struct PointsListResponse: Decodable {
    let points: [PointResponse]
}
