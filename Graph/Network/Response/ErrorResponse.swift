import Foundation

private struct InnerErrorResponse: Decodable {
    let result: Int
    let message: String

    private enum CodingKeys: String, CodingKey {
        case result
        case message
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        result = try container.decode(Int.self, forKey: .result)
        message = try container.decode(String.self, forKey: .message)
    }

}

struct ErrorResponse: Decodable {
    let result: Int
    let message: String

    private enum CodingKeys: String, CodingKey {
        case response
    }

    init(result: Int, message: String) {
        self.result = result
        self.message = message
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let innerError = try container.decode(InnerErrorResponse.self, forKey: .response)
        self.result = innerError.result
        self.message = innerError.message
    }
}
