import Foundation

enum PointDecodableError: Error {
    case typeMismatch
}


struct PointResponse: Decodable {
    let x: Double
    let y: Double

    private enum CodingKeys: String, CodingKey {
        case x
        case y
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let xString = try container.decode(String.self, forKey: .x)
        let yString = try container.decode(String.self, forKey: .y)

        if
            let x = Double(xString),
            let y = Double(yString) {
            self.x = x
            self.y = y
        } else {
            throw PointDecodableError.typeMismatch
        }
    }
}
