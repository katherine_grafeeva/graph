// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name
internal enum L10n {

  internal enum Common {
    /// Error
    internal static let error = L10n.tr("Localizable", "Common.Error")
    /// OK
    internal static let ok = L10n.tr("Localizable", "Common.Ok")
  }

  internal enum EnterPointsNumber {
    /// Done
    internal static let doneButton = L10n.tr("Localizable", "EnterPointsNumber.DoneButton")
    /// Enter number of points:
    internal static let informationText = L10n.tr("Localizable", "EnterPointsNumber.InformationText")
    /// GO!
    internal static let startButton = L10n.tr("Localizable", "EnterPointsNumber.StartButton")
  }

  internal enum Statistics {
    /// Statistics
    internal static let title = L10n.tr("Localizable", "Statistics.Title")
    internal enum PointCell {
      /// Index:
      internal static let indexTitle = L10n.tr("Localizable", "Statistics.PointCell.IndexTitle")
      /// X:
      internal static let xCoordinateTitle = L10n.tr("Localizable", "Statistics.PointCell.XCoordinateTitle")
      /// Y:
      internal static let yCoordinateTitle = L10n.tr("Localizable", "Statistics.PointCell.YCoordinateTitle")
    }
    internal enum SavePhoto {
      internal enum FailureAlert {
        /// Description: %@
        internal static func text(_ p1: String) -> String {
          return L10n.tr("Localizable", "Statistics.SavePhoto.FailureAlert.Text", p1)
        }
        /// Save error
        internal static let title = L10n.tr("Localizable", "Statistics.SavePhoto.FailureAlert.Title")
      }
      internal enum SuccessAlert {
        /// Your image has been saved to your photos.
        internal static let text = L10n.tr("Localizable", "Statistics.SavePhoto.SuccessAlert.Text")
        /// Saved
        internal static let title = L10n.tr("Localizable", "Statistics.SavePhoto.SuccessAlert.Title")
      }
    }
    internal enum SegmentControl {
      /// Chart
      internal static let chart = L10n.tr("Localizable", "Statistics.SegmentControl.Chart")
      /// List
      internal static let list = L10n.tr("Localizable", "Statistics.SegmentControl.List")
    }
  }
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name

// MARK: - Implementation Details

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    // swiftlint:disable:next nslocalizedstring_key
    let format = NSLocalizedString(key, tableName: table, bundle: Bundle(for: BundleToken.self), comment: "")
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

private final class BundleToken {}
