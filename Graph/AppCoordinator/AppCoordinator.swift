import UIKit

enum LaunchUnit {
    enum Event {
        case empty
        case start
    }
}

final class AppCoordinator: CoordinatorType {
    typealias Event = LaunchUnit.Event

    typealias Context = EmptyContext & EnterPointsNumberCoordinator.Context
    private let context: Context

    init(context: Context) {
        self.context = context
    }

    func makeInitial() -> UIViewController {
        return play(event: .start)
    }
}

extension AppCoordinator {
    func play(event: AppCoordinator.Event) -> UIViewController {
        let viewController: UIViewController
        switch event {
        case .empty:
            viewController = UIViewController()
        case .start:
            let navigationController = UINavigationController()
            let enterPointsNumberCoordinator = EnterPointsNumberCoordinator(context: context, container: navigationController)
            navigationController.pushViewController(enterPointsNumberCoordinator.makeInitial(), animated: true)
            viewController = navigationController
        }

        return viewController
    }
}
