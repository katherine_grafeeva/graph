import Foundation


struct Point {
    let x: Double
    let y: Double
}

extension Point {
    init(from response: PointResponse) {
        self.x = response.x
        self.y = response.y
    }
}
