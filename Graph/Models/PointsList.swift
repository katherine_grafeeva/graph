import Foundation


struct PointsList {
    let points: [Point]
}

extension PointsList {
    init(from response: PointsListResponse) {
        self.points = response.points.map({ Point(from: $0) })
    }
}
