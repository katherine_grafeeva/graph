/// A type-erased view model.  It is possible to convert any
/// ViewModelType-compliant object to this type.
///
/// Use this type to form boundaries between view models and view controllers.
final class AnyViewModel<Input, Output>: ViewModelType {
    private let _transform: (Input) -> Output
    init<ViewModel>(_ viewModel: ViewModel) where ViewModel: ViewModelType,
        ViewModel.Input == Input, ViewModel.Output == Output {
            self._transform = viewModel.transform(input:)
    }

    func transform(input: Input) -> Output {
        return _transform(input)
    }
}

extension ViewModelType {
    /// Return type-erased view model for the receiver model.
    func asViewModel() -> AnyViewModel<Input, Output> {
        return .init(self)
    }
}
