protocol RouterType {
    associatedtype Event

    func play(event: Event)
}
