protocol ViewModelType {
    associatedtype Unit: UnitType = DefaultUnit<Input, Output, Never>
        where Unit.Input == Input, Unit.Output == Output
    associatedtype Input
    associatedtype Output

    func transform(input: Input) -> Output
}
