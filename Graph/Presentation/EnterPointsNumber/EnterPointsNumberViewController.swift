import UIKit
import RxSwift
import RxCocoa

struct KeyboardConfiguration {
    let type: UIKeyboardType
}

private struct Constants {

    let font = UIFont.systemFont(ofSize: 20, weight: UIFont.Weight.heavy)

    let pointsNumberTextFieldConfiguration = KeyboardConfiguration(type: .numberPad)
}

private let constants = Constants()

final class EnterPointsNumberViewController: UIViewController {
    typealias Unit = EnterPointsNumberUnit
    var viewModel: EnterPointsNumberUnit.ViewModel?

    @IBOutlet private var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet private var scrollView: UIScrollView!
    @IBOutlet private var informationTextLabel: UILabel!
    @IBOutlet private var pointsNumberTextField: UITextField!
    @IBOutlet private var startButton: UIButton!

    private let bag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        setupKeyboardAvoiding()
        bindViewModel()
    }
}

extension EnterPointsNumberViewController: ViewModelBindable {
    func bind(viewModel: Unit.ViewModel) {
        let input = ViewModel.Input(
            pointsNumberText: pointsNumberTextField.rx.text.orEmpty.asDriver(),
            startTap: startButton.rx.tap.asSignal()
        )
        let output = viewModel.transform(input: input)
        output.isLoading
            .emit(onNext: { [unowned self] isLoading in
                if isLoading {
                    self.activityIndicatorView.startAnimating()
                } else {
                    self.activityIndicatorView.stopAnimating()
                }
            })
            .disposed(by: bag)
        output.isStartButtonEnabled.drive(startButton.rx.isEnabled).disposed(by: bag)
        output.pullcord.emit().disposed(by: bag)
    }
}

private extension EnterPointsNumberViewController {
    func configureView() {
        configureInformationLabel()
        configureStartButton()
        setupInputAccessoryView()

        activityIndicatorView.color = UIColor.blue

        pointsNumberTextField.keyboardType = constants.pointsNumberTextFieldConfiguration.type
    }

    func configureInformationLabel() {
        informationTextLabel.font = constants.font
        informationTextLabel.text = L10n.EnterPointsNumber.informationText
    }

    func configureStartButton() {
        startButton.setBackgroundImage(Asset.Common.Buttons.buttonGreenBackground.image, for: .normal)
        startButton.setBackgroundImage(Asset.Common.Buttons.buttonGreenBackgroundDisabled.image, for: .disabled)
        startButton.setTitle(L10n.EnterPointsNumber.startButton, for: .normal)
    }

    func setupInputAccessoryView() {
        let toolbar = UIToolbar()
        toolbar.barStyle = .default
        toolbar.isTranslucent = true

        let doneItem = UIBarButtonItem(title: L10n.EnterPointsNumber.doneButton, style: .done, target: self, action: #selector(doneTapped))
        let spaceItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolbar.items = [spaceItem, doneItem]
        toolbar.isUserInteractionEnabled = true
        pointsNumberTextField.inputAccessoryView = toolbar
        toolbar.sizeToFit()
    }

    @objc func doneTapped() {
        pointsNumberTextField.resignFirstResponder()
    }

    func setupKeyboardAvoiding() {
        NotificationCenter.default.rx
            .notification(UIResponder.keyboardWillShowNotification)
            .subscribe(onNext: { [weak self] notification in
                guard let keyboardRect = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey]
                    as? NSValue)?.cgRectValue
                    else { return }

                self?.scrollView.contentInset.bottom = keyboardRect.height
                self?.scrollView.scrollIndicatorInsets.bottom = keyboardRect.height
            })
            .disposed(by: bag)

        NotificationCenter.default.rx
            .notification(UIResponder.keyboardWillHideNotification)
            .subscribe(onNext: { [weak self] _ in
                self?.scrollView.contentInset.bottom = 0
                self?.scrollView.scrollIndicatorInsets.bottom = 0
            })
            .disposed(by: bag)
    }
}
