import RxSwift
import RxCocoa


enum EnterPointsNumberUnit: UnitType {
    enum Event {
        case alert(String)
        case show(PointsList)
    }

    struct Input {
        let pointsNumberText: Driver<String>
        let startTap: Signal<Void>
    }

    struct Output {
        let isLoading: Signal<Bool>
        let isStartButtonEnabled: Driver<Bool>
        let pullcord: Signal<Void>
    }
}

final class EnterPointsNumberViewModel {
    typealias Unit = EnterPointsNumberUnit
    typealias Context = PointsContext
    private let context: Context
    private let router: Unit.Router

    init(context: Context, router: Unit.Router) {
        self.context = context
        self.router = router
    }

}

extension EnterPointsNumberViewModel: ViewModelType {
    func transform(input: Unit.Input) -> Unit.Output {
        let errorTracker = PublishRelay<String>()
        let activityIndicator = ActivityIndicator()

        let parameters: HandleStartTapParameters = (input.startTap,
                                                    input.pointsNumberText.map({ Int($0) ?? 0 }),
                                                    errorTracker, activityIndicator)
        let startHandler = handle(parameters: parameters)

        let errors = errorTracker
            .asSignal()
            .map { EnterPointsNumberUnit.Event.alert($0) }
            .route(with: router)
            .map { _ in return }

        let isStartButtonEnabled = input.pointsNumberText.map { Int($0) } .map { $0 != nil }

        let pullcord = Signal.merge([startHandler, errors])
        return Unit.Output(
            isLoading: activityIndicator.asSignal(onErrorSignalWith: .never()),
            isStartButtonEnabled: isStartButtonEnabled,
            pullcord: pullcord
        )
    }
}

private extension EnterPointsNumberViewModel {
    typealias HandleStartTapParameters = (startTap: Signal<Void>,
        pointsNumber: Driver<Int>, errorTracker: PublishRelay<String>, activityIndicator: ActivityIndicator)

    func handle(parameters: HandleStartTapParameters) -> Signal<Void> {
        return parameters.startTap
            .withLatestFrom(parameters.pointsNumber)
            .flatMapLatest({ [pointsService = context.pointsService] pointsNumber -> Signal<ResponseResult<SuccessResponse>> in
                return pointsService.getPoints(count: pointsNumber)
                    .trackActivity(parameters.activityIndicator)
                    .do(onError: { error in
                        parameters.errorTracker.accept(error.localizedDescription)
                    })
                    .asSignal(onErrorSignalWith: .never())
            })
            .do(onNext: { [router] result in
                switch result {
                case let .serverError(error):
                    parameters.errorTracker.accept(error.message)
                case let .success(result):
                    return router.play(event: .show(PointsList(from: result.response)))
                }
            })
            .map { _ in return }
    }
}
