import UIKit


final class EnterPointsNumberCoordinator: CoordinatorType {
    typealias Context = EmptyContext & EnterPointsNumberViewModel.Context
    private let context: Context

    private weak var enterPointsNumberViewController: UIViewController?
    private let container: UINavigationController

    init(context: Context, container: UINavigationController) {
        self.context = context
        self.container = container
    }

    func makeInitial() -> UIViewController {
        let enterPointsNumberViewController = StoryboardScene.EnterPointsNumber.initialScene.instantiate()
        self.enterPointsNumberViewController = enterPointsNumberViewController
        let enterPointsNumberViewModel = EnterPointsNumberViewModel(context: context, router: self.asRouter())
        enterPointsNumberViewController.viewModel = enterPointsNumberViewModel.asViewModel()
        return enterPointsNumberViewController
    }
}


extension EnterPointsNumberCoordinator: RouterType {
    func play(event: EnterPointsNumberUnit.Event) {
        switch event {
        case let .alert(text):
            let alertController = UIAlertController(title: L10n.Common.error, message: text, preferredStyle: .alert)
            let okAction = UIAlertAction(title: L10n.Common.ok, style: .default, handler: { _ in

            })
            alertController.addAction(okAction)

            container.present(alertController, animated: true, completion: nil)
        case let .show(pointsList):
            let coordinator = StatisticsCoordinator(context: context, pointsList: pointsList)
            let viewController = coordinator.makeInitial()
            container.pushViewController(viewController, animated: true)
        }
    }
}

