import UIKit
import RxCocoa
import RxSwift
import Charts


private enum StatisticsViewKind: Int {
    case chart
    case list
}

final class StatisticsViewController: UIViewController {
    typealias Unit = StatisticsUnit
    var viewModel: StatisticsUnit.ViewModel?

    @IBOutlet private var chartView: LineChartView!
    @IBOutlet private var tableView: UITableView!
    @IBOutlet private var segmentControl: UISegmentedControl!

    private let bag = DisposeBag()
    private let saveImage = PublishRelay<UIImage>()

    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
        bindViewModel()

        segmentControl.rx.value.asDriver()
            .map { StatisticsViewKind.init(rawValue: $0) ?? .chart }
            .drive(onNext: { [unowned self] kind in
                switch kind {
                case .chart:
                    self.tableView.isHidden = true
                    self.chartView.isHidden = false
                case .list:
                    self.tableView.isHidden = false
                    self.chartView.isHidden = true
                }
            })
            .disposed(by: bag)
    }
}

extension StatisticsViewController: ViewModelBindable {
    func bind(viewModel: Unit.ViewModel) {
        let input = ViewModel.Input(saveImage: saveImage.asSignal())
        let output = viewModel.transform(input: input)
        output.pullcord.emit().disposed(by: bag)
        output.points
            .drive(tableView.rx.items(cellIdentifier: PointCell.reuseIdentifier, cellType: PointCell.self)) { row, element, cell in
                PointCellConfigurator.configure(cell: cell, with: element, title: String(row))
            }
            .disposed(by: bag)

        output.points
            .drive(onNext: { [unowned self] points in
                self.setDataCount(points: points)
            })
            .disposed(by: bag)

        tableView.reloadData()
    }
}

private extension StatisticsViewController {
    func setDataCount(points: [Point]) {
        let values = points
            .map { point -> ChartDataEntry in
                return ChartDataEntry(x: point.x, y: point.y)
            }

        let set = LineChartDataSet(values: values, label: "")
        set.drawIconsEnabled = false
        set.mode = .cubicBezier

        set.setColor(.lightGray)
        set.setCircleColor(.black)
        set.lineWidth = 1
        set.circleRadius = 3
        set.drawCircleHoleEnabled = false
        set.valueFont = .systemFont(ofSize: 9)
        set.formLineWidth = 1
        set.formSize = 15

        let data = LineChartData(dataSet: set)

        chartView.data = data
    }

    @objc func saveChartTap(_ sender: UIBarButtonItem) {
        let image = chartView.takeScreenshot()
        saveImage.accept(image)
    }
}

// MARK - Configure Appearance

private extension StatisticsViewController {
    func configureView() {
        title = L10n.Statistics.title

        configureNavigationBar()
        configureSegmentControl()
        configureChart()
        configureTableView()
    }

    func configureNavigationBar() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveChartTap))
    }

    func configureSegmentControl() {
        segmentControl.removeAllSegments()
        segmentControl.insertSegment(withTitle: L10n.Statistics.SegmentControl.chart, at: 0, animated: true)
        segmentControl.insertSegment(withTitle: L10n.Statistics.SegmentControl.list, at: 1, animated: true)

        segmentControl.selectedSegmentIndex = 0
    }

    func configureChart() {
        chartView.chartDescription?.enabled = false
        chartView.dragEnabled = true
        chartView.setScaleEnabled(true)
        chartView.pinchZoomEnabled = true

        let leftAxis = chartView.leftAxis
        leftAxis.removeAllLimitLines()

        leftAxis.gridLineDashLengths = [5, 5]
        leftAxis.drawLimitLinesBehindDataEnabled = true

        chartView.rightAxis.enabled = false
        chartView.legend.form = .line

        chartView.isHidden = true
    }

    func configureTableView() {
        tableView.estimatedRowHeight = 150
        tableView.rowHeight = UITableView.automaticDimension

        tableView.register(cellType: PointCell.self)
    }
}

extension UIView {

    func takeScreenshot() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, UIScreen.main.scale)
        drawHierarchy(in: bounds, afterScreenUpdates: true)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image ?? UIImage()
    }
}
