import UIKit
import Reusable

struct PointCellConfigurator {
    static func configure(cell: PointCell, with model: Point, title: String) {
        cell.xValueLabel.text = String(model.x)
        cell.yValue.text = String(model.y)
        cell.titleValueLabel.text = title
    }
}


final class PointCell: UITableViewCell {
    @IBOutlet fileprivate var titleValueLabel: UILabel!
    @IBOutlet fileprivate var xValueLabel: UILabel!
    @IBOutlet fileprivate var yValue: UILabel!

    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var xTitleLabel: UILabel!
    @IBOutlet private var yTitleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()

        configure()
    }
}

extension PointCell: NibReusable {}

private extension PointCell {
    func configure() {
        titleLabel.text = L10n.Statistics.PointCell.indexTitle
        xTitleLabel.text = L10n.Statistics.PointCell.xCoordinateTitle
        yTitleLabel.text = L10n.Statistics.PointCell.yCoordinateTitle
    }
}
