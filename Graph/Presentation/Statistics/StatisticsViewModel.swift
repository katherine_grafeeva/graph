import RxSwift
import RxCocoa


enum StatisticsUnit: UnitType {
    enum Event {
        case alert(title: String, text: String)
    }

    struct Input {
        let saveImage: Signal<UIImage>
    }

    struct Output {
        let points: Driver<[Point]>
        let pullcord: Signal<Void>
    }
}

final class StatisticsViewModel: NSObject, UIImagePickerControllerDelegate {
    typealias Unit = StatisticsUnit
    typealias Context = EmptyContext
    private let context: Context
    private let router: Unit.Router
    private let pointsList: PointsList

    init(context: Context, router: Unit.Router, pointsList: PointsList) {
        self.context = context
        self.router = router
        self.pointsList = pointsList
    }

}

extension StatisticsViewModel: ViewModelType {
    func transform(input: Unit.Input) -> Unit.Output {
        let saveImage = input.saveImage
            .do(onNext: { [weak self] image in
                guard let self = self else {
                    return
                }

                SavePhotoService(router: self.router).save(image: image)
            })
            .map { _ in return }
        let points = pointsList.points.sorted { $0.x < $1.x }
        let pullcord = Signal<Void>.merge([saveImage])
        return Unit.Output(
            points: Driver.just(points),
            pullcord: pullcord
        )
    }


}

class SavePhotoService: NSObject, UIImagePickerControllerDelegate {

    private let router: StatisticsUnit.Router

    init(router: StatisticsUnit.Router) {
        self.router = router
    }

    func save(image: UIImage) {
        UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }

    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            let title = L10n.Statistics.SavePhoto.FailureAlert.title
            let text = L10n.Statistics.SavePhoto.FailureAlert.text(error.localizedDescription)
            router.play(event: .alert(title: title, text: text))
        } else {
            let title = L10n.Statistics.SavePhoto.SuccessAlert.title
            let text = L10n.Statistics.SavePhoto.SuccessAlert.text
            router.play(event: .alert(title: title, text: text))
        }
    }
}

