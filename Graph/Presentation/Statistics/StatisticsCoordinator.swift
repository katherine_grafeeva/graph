import UIKit


final class StatisticsCoordinator: CoordinatorType {
    typealias Context = EmptyContext
    private let context: Context
    private let pointsList: PointsList

    private weak var rootViewController: UIViewController?

    init(context: Context, pointsList: PointsList) {
        self.context = context
        self.pointsList = pointsList
    }

    func makeInitial() -> UIViewController {
        let statisticsViewController = StoryboardScene.Statistics.initialScene.instantiate()
        self.rootViewController = statisticsViewController
        let statisticsViewModel = StatisticsViewModel(context: context, router: self.asRouter(), pointsList: pointsList)
        statisticsViewController.viewModel = statisticsViewModel.asViewModel()
        return statisticsViewController
    }
}

extension StatisticsCoordinator: RouterType {
    func play(event: StatisticsUnit.Event) {
        switch event {
        case let .alert(title, text):
            let alertController = UIAlertController(title: title, message: text, preferredStyle: .alert)
            let okAction = UIAlertAction(title: L10n.Common.ok, style: .default, handler: { _ in

            })
            alertController.addAction(okAction)

            rootViewController?.present(alertController, animated: true, completion: nil)
        }
    }
}

