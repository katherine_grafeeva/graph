import UIKit


@UIApplicationMain
class AppDelegate: UIResponder {

    var window: UIWindow?

    private var handlers: [AppDelegateHandlerType] = []

}

// MARK: - Private functions

private extension AppDelegate {
    func run() -> Bool {
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.makeKeyAndVisible()
        self.window = window

        // Initialize application event handlers.
        let appLauncher = AppLauncherHandler(window: window)
        self.handlers = [appLauncher]

        return true
    }
}

// MARK: - UIApplicationDelegate implementation

extension AppDelegate: UIApplicationDelegate {

    func application(_ application: UIApplication,
                     willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil) -> Bool {
        return run() &&
            handlers.map({ handler in
                handler.application(application, willFinishLaunchingWithOptions: launchOptions)
            }).allTrue()
    }
}

private extension Sequence where Element == Bool {
    func allTrue() -> Bool {
        return self.reduce(true, { (acc, value) -> Bool in
            return acc && value
        })
    }
}


