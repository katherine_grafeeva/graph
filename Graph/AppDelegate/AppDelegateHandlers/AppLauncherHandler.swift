import UIKit


final class AppLauncherHandler {
    typealias Context = EmptyContext & AppCoordinator.Context

    private let context: Context
    private let window: UIWindow

    init(window: UIWindow) {
        let contextBuilder = ContextBuilder()
        let context = contextBuilder.buildContext()

        self.context = context
        self.window = window
    }
}

extension AppLauncherHandler: AppDelegateHandlerType {
    func application(_ application: UIApplication, willFinishLaunchingWithOptions
        launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        run()

        return true
    }

}

// MARK: - Private functions

private extension AppLauncherHandler {
    func run() {
        let appCoordinator = AppCoordinator(context: context)
        let rootVC = appCoordinator.makeInitial()
        window.rootViewController = rootVC
    }
}
