import Foundation


final class AppContext {
    let pointsService: PointsServiceType
    let configuration: Configuration

    init(pointsService: PointsServiceType, configuration: Configuration) {
        self.pointsService = pointsService
        self.configuration = configuration
    }
}

extension AppContext: EmptyContext {}
extension AppContext: PointsContext {}
extension AppContext: ConfigurationContext {}

