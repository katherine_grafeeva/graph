import Foundation

struct Configuration {
    let baseURL = URL(string: "https://demo.bankplus.ru/mobws")!
}

protocol ConfigurationContext: AnyObject {
    var configuration: Configuration {get}
}
