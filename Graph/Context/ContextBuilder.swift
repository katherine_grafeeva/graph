import Foundation


final class ContextBuilder {

    func buildContext() -> AppContext {
        let configuration = Configuration()
        let pointsService = PointsService(with: configuration.baseURL)
        let context = AppContext(pointsService: pointsService, configuration: configuration)
        return context
    }
}
