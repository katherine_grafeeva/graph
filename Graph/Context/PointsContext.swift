import Foundation

protocol PointsContext {
    var pointsService: PointsServiceType {get}
}
